# -*- coding: utf-8 -*-
# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.3.4
#   kernelspec:
#     display_name: Python [conda env:.conda-plotocop]
#     language: python
#     name: conda-env-.conda-plotocop-py
# ---

# + [markdown] Collapsed="false"
# # Scrape arxiv

# + [markdown] Collapsed="false"
# ## Get paper IDs

# + Collapsed="false"
from pathlib import Path
from datetime import date, datetime, timedelta
from time import sleep, mktime
from io import BytesIO
import tempfile
import tarfile
import random
import subprocess
import os
import sys
import logging
import filetype

# Web-related
import requests
import feedparser
import tqdm
import tweepy
import jinja2

# Plot analysis-related
import PIL
import pdf2image
import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from scipy.spatial import cKDTree

# + Collapsed="false"
# Done to avoid DecompressionBombErrors
PIL.Image.MAX_IMAGE_PIXELS = None


# + Collapsed="false"
def preprint_ids(only_fresh=True):
    feed = feedparser.parse("https://export.arxiv.org/rss/cond-mat")

    if feed["bozo"]:  # Fetch failed
        return []

    published = datetime.fromtimestamp(mktime(feed.updated_parsed))
    if only_fresh and datetime.now() - published > timedelta(days=1):
        return []

    preprints = feed["entries"]
    return [
        preprint["id"].split("abs/")[-1]
        for preprint in preprints
        if "UPDATED" not in preprint["title"]
        # TODO: We could check if the new preprint version is improved
    ]


# + [markdown] Collapsed="false"
# ## Download PDFs

# + Collapsed="false"
ALLOWED_EXTS = ["png", "jpg", "jpeg", "tiff"]
IGNORE_EXTS = [
    "tex", "bst", "cls", "bbl", "dat", "rtx", "aux", "bib", "sty", "out", "synctex"
]


def pdf_url(paper_id):
    return f"https://www.arxiv.org/pdf/{paper_id}"


def preprint_url(paper_id):
    return f"https://www.arxiv.org/e-print/{paper_id}"


def response(paper_id):
    """Request response objects corresponding to each preprint of the day"""
    # TODO: use different mirrors randomly for better performance.
    sleep(0.05 + random.uniform(0, 0.1))
    r = requests.get(preprint_url(paper_id))
    if r.status_code == 200:
        return r
    elif r.status_code == 403:
        raise RuntimeError("Rate limit exceeded")
    else:
        raise RuntimeError(f"Unknown error, f{r.status_code}")


def convert_to_rgb(img):
    if img.mode in ["L", "CMYK", "HSV", "RGB", "P"]:
        return img.convert("RGB")
    elif img.mode == "RGBA":
        # Create a white background, and paste the RGBA image
        # on top of it with the alpha channel as the mask
        background = PIL.Image.new("RGB", img.size, (255, 255, 255))
        background.paste(img, mask=img.split()[-1])
        return background
    else:
        logging.warning(f"Unknown image mode {img.mode}")
        return img.convert("RGB")


def paper_images(paths):
    for file in paths:
        if file.suffix[1:] not in ALLOWED_EXTS:
            continue
        if file.stat().st_size < 1000:  # Empty image
            continue

        with PIL.Image.open(file) as image:
            image = convert_to_rgb(image)
            image.thumbnail((200, 200), PIL.Image.Resampling.NEAREST)

            # Check if the image is just a background with a constant color.
            if all(low == high for low, high in image.getextrema()):
                continue

            yield image


def extract_images_pdf(pdf):
    with tempfile.TemporaryDirectory() as tmp_dir:
        root = Path(tmp_dir)
        pdf_file = root / "preprint.pdf"
        pdf_file.write_bytes(pdf)

        subprocess.call(
            f"pdfimages -all {pdf_file} {root}/image",
            shell=True,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
        )

        paths = root.glob("image*")
        yield from paper_images(paths)


def extract_images_tar(tar):
    for member in tar.getmembers():
        if not member.isfile():
            continue
        suffix = Path(member.name).suffix[1:]
        # Check for images with wrong extensions, e.g.
        data = tar.extractfile(member).read()
        guess = filetype.guess(data)
        if guess and guess.extension != suffix:
            suffix = guess.extension
        if suffix in ALLOWED_EXTS:
            with PIL.Image.open(tar.extractfile(member)) as image:
                image = convert_to_rgb(image)
                image.thumbnail((200, 200), PIL.Image.Resampling.NEAREST)
                yield image
        elif suffix == "pdf":
            try:
                image = pdf2image.convert_from_bytes(data, size=200, fmt="ppm")[0]
                yield convert_to_rgb(image)
            except pdf2image.exceptions.PDFPageCountError:
                logging.warning(f'Broken pdf image {member.name}')
        elif suffix in IGNORE_EXTS:
            pass
        else:
            logging.warning(f"Unknown extension {suffix}")


def extract_images(response):
    content_type = response.headers["Content-Type"]
    if content_type == "application/pdf":
        yield from extract_images_pdf(response.content)
    elif content_type == "application/x-eprint-tar":
        with tarfile.open(fileobj=BytesIO(response.content)) as tar:
            yield from extract_images_tar(tar)
    elif content_type != "application/x-eprint":
        logging.warning(f"Unknown content type {content_type}")


# + [markdown] Collapsed="false"
# # Colormap detection

# + Collapsed="false"
jet_fn = cm.get_cmap("jet")
jet_pts = [jet_fn(x) for x in np.linspace(0, 1, 256)]
jet_pts = np.array(jet_pts)[:, :3]
JET_TREE = cKDTree(jet_pts)


def points_near_jet(img, r=0.2, plot=False):
    # Represent image as float so we can perform division
    img = np.array(img).astype(float).reshape(-1, 3)
    img /= 255  # Map colors into a unit cube.
    img_tree = cKDTree(img, balanced_tree=False, compact_nodes=False)
    pts = [res for neighs in JET_TREE.query_ball_tree(img_tree, r) for res in neighs]
    pts = np.unique(pts)

    if plot:
        from mpl_toolkits.mplot3d import Axes3D

        fig = plt.figure()
        ax = fig.add_subplot(111, projection="3d")
        ax.plot(*jet_pts.T, lw=10, label="Jet colormap")
        ax.scatter(*img.T, s=5, c="C1", label="RGB points of figure")
        plt.legend()
        plt.show()

    return pts


def label_image(pts, w, h):
    labeled_img = np.zeros((h, w), dtype=np.uint8)
    if len(pts):
        inds = np.unravel_index(pts, (h, w))
        labeled_img[inds] = 1
    return labeled_img


def contour_mask(w, h, cnt):
    empty_img = np.zeros((h, w, 3))
    mask = cv2.drawContours(empty_img, [cnt], 0, (0, 1, 0), -1)
    mask = np.nonzero(mask)
    return mask[:2]


def bounding_rect_slice(cnt):
    x, y, w, h = cv.boundingRect(cnt)
    return slice(y, y + h), slice(x, x + w)


def contains_jet(pts, r_max):
    pts = np.float64(pts) / np.max(pts)
    dists = np.linalg.norm(pts[None, :, :] - jet_pts[:, None, :], axis=-1)
    av_min_dist = np.average(np.min(dists, axis=1))
    return av_min_dist < r_max


# + [markdown] Collapsed="false"
# # Run detection

# + Collapsed="false"
def find_jet_papers(todays_ids):
    jet_papers = []

    for paper_id in tqdm.tqdm(todays_ids):
        found_jet = False
        for img in extract_images(response(paper_id)):
            with img:
                candidates = points_near_jet(img, 0.2)
                labeled_image = label_image(candidates, *img.size)
                contours, hierarchy = cv2.findContours(
                    labeled_image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
                )
                contours = [cnt for cnt in contours if cv2.contourArea(cnt) > 100]
                masks = [contour_mask(*img.size, cnt) for cnt in contours]
                segments = [np.array(img)[mask] for mask in masks]

                for seg in segments:
                    if contains_jet(seg, 0.05):
                        found_jet = True
                        break
                if found_jet:
                    jet_papers.append(paper_id)
                    break
    
    return jet_papers


# + [markdown] Collapsed="false"
# # Tweet stuff
# -

def connect_to_api():
    # Authenticate to Twitter
    auth = tweepy.OAuthHandler(os.environ["API_KEY"], os.environ["API_SECRET_KEY"])
    auth.set_access_token(os.environ["ACCESS_TOKEN"], os.environ["ACCESS_TOKEN_SECRET"])

    # Create API object
    return tweepy.API(auth)


# + Collapsed="false"
MAX_TWEET_LENGTH = 280
URL_LENGTH = 23

# + Collapsed="false"
# {violator amount: [possible responses]}

evaluations = {
    0: [
        "NO JET USAGE DETECTED☮",
        "MUST STAY VIGILANT",
        "THIS MARKS THE BEGINNING OF A NEW ERA OF PEACE AND PROSPERITY",
    ],
    1: ["VIOLATION REPORTED", "ISSUING A WARNING NOTICE",],
    2: ["UPDATING PLOT CRIMINAL RECORDS", "DISPATCHING CAM02-UCS UNITS",],
    3: [
        "PATROL UNITS SUSTAINED NO PERMANENT DAMAGE",
        "AREA QUARANTINED",
        "VISUAL CONTACT ESTABLISHED",
    ],
    4: [
        "SUSPECTED COORDINATED JET ATTACK",
        "WARNING: JET OVERFLOW IMMINENT",
        "VISCM DETECTOR DAMAGED",
    ],
    5: ["PLOT CRIME RUNS RAMPANT", "REQUESTING IMMEDIATE BACKUP",],
    np.inf: [
        "THERE ARE TOO MANY OF THEM",
        "EXTREME JET CONTAMINATION, SEEK SHELTER",
        "DECLARING GLOBAL STATE OF PLOT EMERGENCY",
    ],
}

# + Collapsed="false"
# Limit the number of preprints to 14, so that we don't exceed the max tweet length.

status_report = jinja2.Template(
    """arXiv/cond-mat STATUS REPORT {{date.strftime('%d/%m/%Y')}}
🚨🚨 JET DETECTED IN {{ jet_papers | length }} / {{ todays_ids | length }} PREPRINTS 🚨🚨{{ ': ' if jet_papers else '.' }}
{{- paper_links }}

{{ description }}
"""
)


# + Collapsed="false"
def prepare_tweet(todays_ids, jet_papers):
    status_options = next(
        options for number, options in evaluations.items() if number >= len(jet_papers)
    )
    description = random.choice(status_options)

    # Compute how much space we have remaining
    bare_length = len(
        status_report.render(
            date=date.today(),
            jet_papers=jet_papers,
            description=description,
            todays_ids=todays_ids,
        )
    )

    remaining_chars = MAX_TWEET_LENGTH - bare_length
    separator = ", "
    remaining_links = (remaining_chars + len(separator)) // (
        URL_LENGTH + len(separator)
    )
    if len(jet_papers) <= max(4, remaining_links):
        # We may link to all papers
        paper_links = separator.join(pdf_url(paper_id) for paper_id in jet_papers)
    else:
        # We just list the preprint ids.
        remaining_papers = (remaining_chars + len(separator)) // (9 + len(separator))
        if len(jet_papers) > remaining_papers:
            paper_links = separator.join(jet_papers[: remaining_papers - 1] + ["…"])
        else:
            paper_links = separator.join(jet_papers[:remaining_papers])

    return status_report.render(
        date=date.today(),
        jet_papers=jet_papers,
        description=description,
        paper_links=paper_links,
        todays_ids=todays_ids,
    )


# + Collapsed="false"
if __name__ == "__main__":
    todays_ids = preprint_ids()

    if not todays_ids:
        sys.exit()

    jet_papers = find_jet_papers(todays_ids)
    tweet = prepare_tweet(todays_ids, jet_papers)

    print(jet_papers)
    print(tweet)
    connect_to_api().update_status(tweet)
# -


