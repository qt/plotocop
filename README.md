# Plot police planning

## Aims

* Study plotting traditions
* Promote good plotting practices
* Learn fun algorithms and tools
* Engage community
* Have fun

### Non-goals

* Anything related to manuscripts but not figures (data/text/etc)
* Anything that cannot be automated to a large extent

### Openness

#### Pros

* Easier for others to extend
* Easy to engage community
* Visibility

#### Cons

* Cannot go full troll mode
* Risks of being banned by arXiv?
    > [name=Anton Akhmerov] Probably not: they can't ban by codebase, and there's a bunch of others doing the same.

#### Conclusion

We go OSS

## Possible milestones

### Publication formats

* Website with explanations
* Twitter bot
* Dashboard/statistics
* Automated emails

### Analysis

* Colormap matching
* Colormap detection
* Colorbar detection
* Label content, position, and size detection
* Data range detection
* Panel detection
* Line style detection
* Automated colormap quality using `viscm`
* Plot disk size/performance
* Amount of information (could be a whole bunch of things)

### Features

* Replotting
* Missing colorbars
* Tweet responses/twitter search
* Data storage
* Bulk analysis (arXiv via S3, costs about 100€)
* Finding good plots

## MVP

* Jet detection
* Twitter bot tweeting jet/viridis figure previews
* No storage yet

### Components

* Gitlab 